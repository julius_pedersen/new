var gulp = require('gulp'),
    concat = require('gulp-concat');

gulp.task('shipStatic', function(done) {
    /*
     * index.html -> /dist/index.html
     * lib/js/* -> /dist/static/js/*
     * lib/css/* -> /dist/static/css/*
     */
    gulp.src('./index.html')
        .pipe(gulp.dest('./dist/'));
    gulp.src('./lib/**/*.js')
        .pipe(gulp.dest('./dist/static/'));
    gulp.src('./lib/**/*.css')
        .pipe(gulp.dest('./dist/static/'));

    done();
});

gulp.task('shipJS', function(done) {
    /*
     * Concatenates all project js files into dist/static/js/app.js
     */
    gulp.src([
        // Third party
        './node_modules/jquery/dist/js/jquery.js',
        './node_modules/materialize-css/dist/js/materialize.js',
        './node_modules/vue/dist/js/vue.js',
        './node_modules/vue-router/dist/vue-router.js',
        // Project
        './app/**/service/*.js',
        './app/**/component/*.js',
        './app/**/*.js',
    ]).pipe(concat('app.js'))
      .pipe(gulp.dest('./dist/static/js/'));

    done();
});

gulp.task('shipCSS', function(done) {
    /*
     * Concatenates all project css files into dist/static/css/app.css
     */
    gulp.src([
        // Third party
        './node_modules/materialize-css/dist/css/materialize.min.css',
        // Project
        './app/**/*.css',
    ]).pipe(concat('app.css'))
      .pipe(gulp.dest('./dist/static/css/'));

    done();
});

gulp.task('make', gulp.parallel('shipStatic', 'shipJS', 'shipCSS', function(done) {
    done();
}));

gulp.task('watch', function(done) {
    gulp.watch('./app/**/*.js', gulp.parallel('make'));
    gulp.watch('./app/**/*.css', gulp.parallel('make'));
    gulp.watch('./index.html', gulp.parallel('make'));

    done();
});
